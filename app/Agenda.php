<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = ['fecha_cita', 'sucursal_id', 'mecanico_id', 'cliente_id'];

    public function Sucursal()
    {
        return $this->belongsTo('App\Sucursal');
    }

    public function Mecanico()
    {
        return $this->belongsTo('App\Mecanico');
    }

    public function Cliente()
    {
        return $this->belongsTo('App\Cliente');
    }
    
}
