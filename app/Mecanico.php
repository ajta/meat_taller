<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mecanico extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'email', 'sucursal_id'];

    public function Sucursal()
    {
        return $this->belongsTo('App\Sucursal');
    }

    public function Agenda()
    {
        return $this->hasMany('App\Agenda');
    }
}
