<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursales';

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'email', 'sucursal_id'];

    
}
