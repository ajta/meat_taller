<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App\Agenda;
use App\Mecanico;
use App\Sucursal;
use App\Cliente;
use Illuminate\Http\Request;

class AgendaControl extends Controller
{
    public function store(Request $request)
    {
        /*VALIDACION DE LOS DATOS */
        $validatedData = $request->validate([
            'fecha_cita' => ['required', 'date_format:Y-m-d H:i'],
            'sucursal_id' => ['required'],
            'mecanico_id' => ['required'],
            'nombre' => ['required'],
            'email' => ['required', 'email'],
        ]);
        /* VALIDAR QUE EXISTA LA SUCURSAL INDICADA */
        $sucursal = Sucursal::find($request->sucursal_id);
        if($sucursal == null){
            return response()->json(['ERROR' => 'NO EXISTE LA SUCURSAL'], 400);
        }

        /* VALIDAR QUE EXISTA EL MECANICO */
        $mecanico = Mecanico::find($request->mecanico_id);
        if($mecanico == null){
            return response()->json(['ERROR' => 'NO EXISTE EL MECANICO'], 400);
        }
        
        /*VALIDACION O CREACION DE CLIENTE */
        $cliente = Cliente::where('email', $request->email)->firstOr(function () use($request){
            return Cliente::create(['email'=>$request->email, 'nombre'=>$request->nombre]);
        });
        
        /*VALIDACION DE HORARIOS Y DISPONIBILIDAD*/
        $ValidarHora = $this -> ValidarHora($mecanico, $request->fecha_cita);
        if (!$ValidarHora['valid']) {
            return response()->json(['ERROR' => $ValidarHora['msg']], 400);
        }

        /*SI PASA TODAS LAS VALIDACIONES SE AGENDA */
        $agenda = new Agenda;
        $agenda->fecha_cita = $request->fecha_cita;
        $agenda->Sucursal()->associate($sucursal);
        $agenda->Mecanico()->associate($mecanico);
        $agenda->Cliente()->associate($cliente);
        $agenda->save();

        return response()->json($agenda, 201);
    }

    static function ValidarHora($mecanico = null, $fecha_cita = null){
        $fecha_cita = Carbon::parse($fecha_cita);
        $fecha_min = $fecha_cita->copy();
        $fecha_max = $fecha_cita->copy();
        $fecha_min->hour = 13;//fijamos la hora minima del dia en las 13 para luego comparar
        $fecha_min->minute = 0;
        $fecha_max->hour = 15;//fijamos la hora maxima del dia en las 15 para luego comparar
        $fecha_max->minute = 0;

        /*VALIDAR DIA DE SEMANA */
        if (!$fecha_cita->isWeekday()) {
            return ['valid'=>false, 'msg'=>'La fecha ingresada no corresponde a dia de trabajo'];
        }
        /* VALIDAR HORARIO DE TRABAJO */
        if (!$fecha_cita->between($fecha_min, $fecha_max)) {
            return ['valid'=>false, 'msg'=>'La fecha ingresada se encuentra fuera de horario'];
        }

        /*VALIDAR FECHA VS FECHAS AGENDADAS */
        $agenda = $mecanico->Agenda;
        if($agenda->count()>0){
            /*RECORREMOS LAS CITAS YA AGENDADAS DEL MECANICO */
            foreach ($agenda as $key => $value) {
                $fecha_agandada = Carbon::parse($value->fecha_cita);//tomamos la hora agendada 
                $fecha_agandada_max = $fecha_agandada->copy()->addMinutes(20);// le sumamos 20 min para tener un maximo de hora para esa cita
                
                /*SE COMPARA LA FECHA INGRESADA CON LA DE LA CITA YA REGISTRADA */
                if ($fecha_cita->between($fecha_agandada, $fecha_agandada_max)) {
                    return ['valid'=>false, 'msg'=>'La fecha ingresada se encuentra en un horario ocupado.'];
                }
                
            }
        }

        /*PASO LAS VALIDACIONES */
        return ['valid'=>true, 'msg'=>'Valido'];
    }
}
