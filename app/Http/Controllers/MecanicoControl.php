<?php

namespace App\Http\Controllers;
use App\Mecanico;
use App\Sucursal;
use Illuminate\Http\Request;

class MecanicoControl extends Controller
{
    public function index()
    {
        return Mecanico::all();
    }

    public function show(Mecanico $mecanico)
    {
        /*VALIDACION DE LOS DATOS */
        $validatedData = $request->validate([
            'id' => ['required']
        ]);
        return $mecanico;
    }

    public function store(Request $request)
    {   
        /*VALIDACION DE LOS DATOS */
        $validatedData = $request->validate([
            'email' => ['required', 'email', 'unique:mecanicos,email'],
            'nombre' => ['required'],
            'sucursal_id' => ['required'],
        ]);

        /* VALIDAR QUE EXISTA LA SUCURSAL INDICADA */
        $sucursal = Sucursal::find($request->sucursal_id);
        if($sucursal == null){
            return response()->json(['ERROR' => 'NO EXISTE LA SUCURSAL'], 400);
        }

        $mecanico = new Mecanico;
        $mecanico->nombre = $request->nombre;
        $mecanico->email = $request->email;
        $mecanico->Sucursal()->associate($sucursal);
        $mecanico->save();

        return response()->json($mecanico, 201);
    }

    
}
