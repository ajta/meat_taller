<?php

namespace App\Http\Controllers;
use App\Sucursal;
use Illuminate\Http\Request;

class SucursalControl extends Controller
{
    public function index()
    {
        return Sucursal::all();
    }

    public function show(Sucursal $sucursal)
    {
        /*VALIDACION DE LOS DATOS */
        $validatedData = $request->validate([
            'id' => ['required']
        ]);
        
        return $sucursal;
    }

    public function store(Request $request)
    {
        /*VALIDACION DE LOS DATOS */
        $validatedData = $request->validate([
            'nombre' => ['required']
        ]);
        $sucursal = Sucursal::create($request->all());

        return response()->json($sucursal, 201);
    }

    public function delete(Sucursal $sucursal)
    {
        /*VALIDACION DE LOS DATOS */
        $validatedData = $request->validate([
            'id' => ['required']
        ]);
        $sucursal->delete();

        return response()->json(['MSG'=>'Se proceso la solicitud'], 204);
    }
}
