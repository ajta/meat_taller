<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'email'];

    public function job_details(){
          return $this->belongsToMany('App\job_details');
    }
}
