<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('fecha_cita');
            $table->unsignedInteger('sucursal_id');
            $table->unsignedInteger('mecanico_id');
            $table->unsignedInteger('cliente_id');

            /*LLAVES FORENEAS */
            $table->foreign('sucursal_id')->references('id')->on('sucursales');
            $table->foreign('mecanico_id')->references('id')->on('mecanicos');
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
