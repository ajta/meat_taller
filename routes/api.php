<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*Rutas Sucursal */
Route::get('sucursales', 'SucursalControl@index');
Route::get('sucursal/{sucursal}', 'SucursalControl@show');

/*Parametros store:
'nombre' => txt,
*/
Route::post('sucursal', 'SucursalControl@store');
Route::delete('sucursal/{sucursal}', 'SucursalControl@delete');

/*Rutas Mecanico */
Route::get('mecanicos', 'MecanicoControl@index');
Route::get('mecanico/{article}', 'MecanicoControl@show');
/*Parametros store:
'nombre' => txt,
'email' => txt,
*/
Route::post('mecanico', 'MecanicoControl@store');

/*Agendar */
/*Parametros:
'fecha_cita' =>  'date_format:Y-m-d H:i'],
'sucursal_id' => txt,
'mecanico_id' => txt,
'nombre' => txt,
'email' => txt
*/
Route::post('agendar', 'AgendaControl@store');